# Starting docker containers
##### Установка - `sudo apt install curl -y && curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh`
##### Создание директории для данных - `mkdir ~/container-data`
#### ARM64v8
##### Загрузка образов - `sudo docker pull syegres/l-band-arm:first`
##### Запуск записи - `sudo docker run -it --privileged --rm -v ~/container-data:/tracks syegres/l-band-arm:first`
#### AMD64
##### Загрузка образов - `sudo docker pull syegres/l-band:first`
##### Запуск записи - `sudo docker run -it --privileged --rm -v ~/container-data:/tracks syegres/l-band:first`
