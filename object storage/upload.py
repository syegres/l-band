import boto3
import os

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

receiver = 'planum-0'
dir = '20220708_062427_NOAA 19'

for root, dirs, files in os.walk(dir):
    for file in files:
        s3.upload_file(os.path.join(root, file), 'l-band.yegres.tk', os.path.join(receiver, root, file).replace('\\', '/'))
        print(os.path.join(root, file), 'l-band.yegres.tk', os.path.join(receiver, root, file))

